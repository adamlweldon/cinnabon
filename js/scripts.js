$(document).ready(function(){

	$('.belt').slick({
		slidesToScroll: 1,
		centerMode: true,
  		slidesToShow: 4,
  		autoplay: true,
  		autoplaySpeed: 0,
  		speed: 5000,
  		cssEase: 'linear',
  		infinite: true,
  		arrows: false,
  		ease: 'linear',
  		pauseOnFocus: false,
  		pauseOnHover: false,
  		responsive: [
  			{
  				breakpoint: 767,
  				settings: {
  					slidesToShow: 2,
  				}
  			},
  			{
  				breakpoint: 550,
  				settings: {
  					slidesToShow: 1,
  				}
  			}
  		]
	});

	// $('nav section').on('click', function(){
	// 	$(this).find('.toggle-trigger').one().trigger('click');
	// });

	$('nav section').on('click', function(){
	// $('.toggle-trigger').on('click', function(){
		var targeted =  $(this).attr('data-attr');

		$('header').addClass('hideit');
		$('html, body').delay(300).animate({ scrollTop: 0 }, 100);

		if ($(this).find('.toggle-trigger').hasClass('turn')) {
			$(this).find('.toggle-trigger').removeClass('turn');

			if ($('.hide').hasClass('now')) {
				setTimeout(function() {
					$('.hide').removeClass('now');
				}, 600);
			}
			else{
				setTimeout(function() {
					$('.hide').addClass('now');
				}, 600);
			}
		}
		else{
			$('.toggle-trigger').removeClass('turn');
			$(this).find('.toggle-trigger').addClass('turn');
			setTimeout(function() {
				$('.hide').addClass('now');
			}, 600);
		}

		if ($('#'+targeted).hasClass('show')) {
			setTimeout(function() {
				$('#'+targeted).removeClass('show');
			}, 600);
		}
		else{
			setTimeout(function() {
				$('.each-one').removeClass('show');
				$('#'+targeted).addClass('show');
				if ($(window).width()>991) {
					var fullheight = $(window).height(),
						topheight = $('#'+targeted+' .each-content').height() + 50;
					$('#'+targeted+' .each-img').css({
						'height' : fullheight - topheight
					});
				}
			}, 600);
		}

		if ($(window).width()>991) {

			setTimeout(function() {
				var heightmatch = $('.each-one.show .each-content').height() + 50;
				$('.each-one').css({
					'min-height' : 0
				})
				$('.each-one.show').css({
					'min-height' : heightmatch
				});
			}, 900);
		}
		
		if (targeted == 'oreo-cookies-cream-chillatta') {
			if ($('p.showoreo').hasClass('intime')) {
				setTimeout(function() {
					$('p.showoreo').removeClass('intime');
				}, 1000);
			}
			else{
				setTimeout(function() {
					$('p.showoreo').addClass('intime');
				}, 1000);
			}
			
		}else{
			setTimeout(function() {
				$('p.showoreo').removeClass('intime');
			}, 1000);
		}
		

		// change to if div loaded
		setTimeout(function() {
       		$('header.hideit').removeClass('hideit');
   		}, 1500);
	});


	if ($(window).width()>991) {
		$(window).resize(function(){
			if ($('.each-one').hasClass('show')) {
				var fullheight = $(window).height(),
					topheight = $('.each-one.show .each-content').height() + 50;
				$('.each-one.show .each-img').css({
					'height' : fullheight - topheight
				});
			}
			
		});
	}

});