module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: {
					'styles.css' : 'sass/style.scss',
					// 'media.css' : 'sass/media.scss',
				},
				options: {
					loadPath: 'sass/',
					style: 'compressed'
				}
			}
		},
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: 'style.css'
            }
        },
		watch: {
			css: {
				files: ['sass/*.scss', 'sass/_partials/*.scss'],
				// tasks: ['sass']
				tasks: ['sass', 'postcss']
			}
		}
	});
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('dev',['watch']);
};