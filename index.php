<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!-- opengraph -->
	<meta property="og:title" content="What's Your Flavor? | Cinnabon" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://whatsyourflavor.cinnabon.com" />
	<meta property="og:image" content="assets/img/cinnabon.png" />
	<meta property="og:description" content="Choose your favorite Chillatta&#8482; flavor!" />

	<title>What's Your Flavor? | Chillatta™ | Cinnabon</title>
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="assets/img/favicon/manifest.json">
	<link rel="mask-icon" href="assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" href="styles.css">

	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-87757352-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WZV7CTV');</script>

</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WZV7CTV"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<header>
		<a href="/">
			<img src="assets/img/cinnabon-new.svg" alt="Cinnabon">
		</a>
	</header>

	<main>

		<!-- conveyor belt -->
		<section class="conveyor gradient">
			<div class="hide">
				<h1>What's Your <span class="fancy">flavor?</span></h1>
				<div class="belt">
					<div>
						<img src="assets/img/cinnabon.png" alt="Cinnamon Roll Chillatta">
					</div>
					<div>
						<img src="assets/img/strawberry.png" alt="Strawberries & Cream Chillatta">
					</div>
					<div>
						<img src="assets/img/oreo.png" alt="Oreos Cookies & Cream Chillatta">
					</div>
					<div>
						<img src="assets/img/peach.png" alt="Peaches & Cream Chillatta">
					</div>
					<div>
						<img src="assets/img/chocolate.png" alt="Double Chocolate Mocha Chillatta">
					</div>
				</div>
			</div>
		</section>

		<!-- each chillatta -->

		<!-- chocolate -->
		<article class="each-one" id="double-chocolate-mocha-chillatta">
			<div class="width-50">
				<div class="full-img gradient">
					<img src="assets/img/chocolate.png" alt="Double Chocolate Mocha Chillatta">
					<img src="assets/img/ghiradelli-logo.png" alt="Ghiradelli Logo" class="ghir-logo">
					<h1 class="visible-sm visible-xs">Double Chocolate Mocha</h1>
				</div>
			</div>
			<div class="width-50 fix-help">
				<div class="each-content hidden-sm">
					<div class="pad">
						<h1 class="hidden-sm hidden-xs">Double Chocolate Mocha</h1>
						<h3>Chillatta<span class="tm">&#8482;</span></h3>
						<p>The most passionate of all the flavors, you are deep and thoughtful. You hold an inner strength and have the ability to bring out the best in others. You fall easily because true happiness to you is to be rich in love. <a href="https://www.cinnabon.com/bakery-menu/chillatta/chocolate-mocha-chillatta">More info</a></p>
						<ul><div>Share your flavor</div> 
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/chocolate-mocha-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Chocolate%20Mocha%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fchocolate-mocha-chillatta&hashtags=DoubleChocolateMocha"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
							<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="each-img" id="chocolate">
					<div class="bg">
						<div class="pad visible-sm">
							<h1 class="hidden-sm hidden-xs">Double Chocolate Mocha</h1>
							<h3>Chillatta<span class="tm">&#8482;</span></h3>
							<p>The most passionate of all the flavors, you are deep and thoughtful. You hold an inner strength and have the ability to bring out the best in others. You fall easily because true happiness to you is to be rich in love. <a href="https://www.cinnabon.com/bakery-menu/chillatta/chocolate-mocha-chillatta">More info</a></p>
							<ul><div>Share your flavor</div> 
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/chocolate-mocha-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Chocolate%20Mocha%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fchocolate-mocha-chillatta&hashtags=DoubleChocolateMocha"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
								<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="scroll-next"></div>
			<div class="scroll-next-shadow"></div>

		</article>

		<!-- peaches -->
		<article class="each-one" id="peaches-cream-chillatta">
			<div class="width-50">
				<div class="full-img gradient">
					<img src="assets/img/peach.png" alt="Peaches & Cream Chillatta">
					<h1 class="visible-sm visible-xs">Peaches & Cream</h1>
				</div>
			</div>
			<div class="width-50 fix-help">
				<div class="each-content hidden-sm">
					<div class="pad">
						<h1 class="hidden-sm hidden-xs">Peaches & Cream</h1>
						<h3>Chillatta<span class="tm">&#8482;</span></h3>
						<p>You are charming, the all-American, sweet and innocent type but there’s more to you than meets the eye. Just like your Chillatta, you’ve got many layers and though you can be a little shy, those that take the time to get to the bottom of you will find the very best part. <a href="https://www.cinnabon.com/bakery-menu/chillatta/peaches-cream-chillatta">More info</a></p>
						<ul><div>Share your flavor</div> 
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3//www.cinnabon.com/bakery-menu/chillatta/peaches-cream-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Peaches%20Cream%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fpeaches-cream-chillatta&hashtags=PeachesAndCream"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
							<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="each-img" id="peach">
					<div class="bg">
						<div class="pad visible-sm">
							<h1 class="hidden-sm hidden-xs">Peaches & Cream</h1>
							<h3>Chillatta<span class="tm">&#8482;</span></h3>
							<p>You are charming, the all-American, sweet and innocent type but there’s more to you than meets the eye. Just like your Chillatta, you’ve got many layers and though you can be a little shy, those that take the time to get to the bottom of you will find the very best part. <a href="https://www.cinnabon.com/bakery-menu/chillatta/peaches-cream-chillatta">More info</a></p>
							<ul><div>Share your flavor</div> 
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3//www.cinnabon.com/bakery-menu/chillatta/peaches-cream-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Peaches%20Cream%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fpeaches-cream-chillatta&hashtags=PeachesAndCream"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
								<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="scroll-next"></div>
			<div class="scroll-next-shadow"></div>

		</article>

		<!-- oreo -->
		<article class="each-one" id="oreo-cookies-cream-chillatta">
			<div class="width-50">
				<div class="full-img gradient">
					<img src="assets/img/oreo.png" alt="Oreo Cookies & Cream Chillatta">
					<img src="assets/img/oreo-logo.png" alt="Oreo Logo" class="oreo-logo">
					<h1 class="visible-sm visible-xs">Oreo<span class="reg">®</span> Cookies & Cream</h1>
				</div>
			</div>
			<div class="width-50 fix-help">
				<div class="each-content hidden-sm">
					<div class="pad">
						<h1 class="hidden-xs hidden-sm">Oreo<span class="reg">®</span> Cookies & Cream</h1>
						<h3>Chillatta<span class="tm">&#8482;</span></h3>
						<p>Although you can sometimes be seen as a little hard on the outside, you’re really a big softy filled with goodness.  You have a huge circle of friends and you’re everyone’s favorite probably because of your sense of humor. You’re a success, the leader of any pack, bringing others together. <a href="https://www.cinnabon.com/bakery-menu/chillatta/oreo-chillatta">More info</a></p>
						<ul><div>Share your flavor</div> 
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/oreo-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Oreo®%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Foreo-chillatta&hashtags=Oreo"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
							<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="each-img" id="oreo">
					<div class="bg">
						<div class="pad visible-sm">
							<h1 class="hidden-xs hidden-sm">Oreo<span class="reg">®</span> Cookies & Cream</h1>
							<h3>Chillatta<span class="tm">&#8482;</span></h3>
							<p>Although you can sometimes be seen as a little hard on the outside, you’re really a big softy filled with goodness.  You have a huge circle of friends and you’re everyone’s favorite probably because of your sense of humor. You’re a success, the leader of any pack, bringing others together. <a href="https://www.cinnabon.com/bakery-menu/chillatta/oreo-chillatta">More info</a></p>
							<ul><div>Share your flavor</div> 
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/oreo-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Oreo®%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Foreo-chillatta&hashtags=Oreo"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
								<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="scroll-next"></div>
			<div class="scroll-next-shadow"></div>

		</article>

		<!-- strawberries -->
		<article class="each-one" id="strawberries-cream-chillatta">
			<div class="width-50">
				<div class="full-img gradient">
					<img src="assets/img/strawberry.png" alt="Strawberries & Cream Chillatta">
					<h1 class="visible-sm visible-xs">Strawberries & Cream</h1>
				</div>
			</div>
			<div class="width-50 fix-help">
				<div class="each-content hidden-sm">
					<div class="pad">
						<h1 class="hidden-xs hidden-sm">Strawberries & Cream</h1>
						<h3>Chillatta<span class="tm">&#8482;</span></h3>
						<p>You’ve got a big personality that brightens everyone’s day. Fiery and fun-loving, people always stop and take notice. Wild at heart, you’re always looking for ways to make life juicier and are ready whenever adventure calls. <a href="https://www.cinnabon.com/bakery-menu/chillatta/strawberry-chillatta">More info</a></p>
						<ul><div>Share your flavor</div> 
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/strawberry-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Strawberry%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fstrawberry-chillatta&hashtags=StrawberriesAndCream"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
							<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="each-img" id="strawberry">
					<div class="bg">
						<div class="pad visible-sm">
							<h1 class="hidden-xs hidden-sm">Strawberries & Cream</h1>
							<h3>Chillatta<span class="tm">&#8482;</span></h3>
							<p>You’ve got a big personality that brightens everyone’s day. Fiery and fun-loving, people always stop and take notice. Wild at heart, you’re always looking for ways to make life juicier and are ready whenever adventure calls. <a href="https://www.cinnabon.com/bakery-menu/chillatta/strawberry-chillatta">More info</a></p>
							<ul><div>Share your flavor</div> 
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/strawberry-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Strawberry%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fstrawberry-chillatta&hashtags=StrawberriesAndCream"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
								<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="scroll-next"></div>
			<div class="scroll-next-shadow"></div>

		</article>

		<!-- cinnamon -->
		<article class="each-one" id="signature-cinnamon-roll-chillatta">
			<div class="width-50">
				<div class="full-img gradient">
					<img src="assets/img/cinnabon.png" alt="Signature Cinnamon Roll Chillatta">
					<h1 class="visible-sm visible-xs">Signature Cinnamon Roll</h1>
				</div>
			</div>
			<div class="width-50 fix-help">
				<div class="each-content hidden-sm">
					<div class="pad">
						<h1 class="hidden-xs hidden-sm">Signature Cinnamon Roll</h1>
						<h3>Chillatta<span class="tm">&#8482;</span></h3>
						<p>You are warm and compassionate and bring joy to those around you. People have always thought you’re something special and you’re known for your classic good taste.  Truly an optimist, you make the best of every situation and roll with whatever comes your way.  When the world tries to bring you down, you prefer to look at the sweeter side of life! <a href="https://www.cinnabon.com/bakery-menu/chillatta/cinnamon-roll-coffee-chillatta">More info</a></p>
						<ul><div>Share your flavor</div> 
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/cinnamon-roll-coffee-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a></li>
							<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Cinnamon%20Roll%20Coffee%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fcinnamon-roll-coffee-chillatta&hashtags=CinnamonRoll"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
							<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="each-img" id="cinnabon">
					<div class="bg">
						<div class="pad visible-sm">
							<h1 class="hidden-xs hidden-sm">Signature Cinnamon Roll</h1>
							<h3>Chillatta<span class="tm">&#8482;</span></h3>
							<p>You are warm and compassionate and bring joy to those around you. People have always thought you’re something special and you’re known for your classic good taste.  Truly an optimist, you make the best of every situation and roll with whatever comes your way.  When the world tries to bring you down, you prefer to look at the sweeter side of life! <a href="https://www.cinnabon.com/bakery-menu/chillatta/cinnamon-roll-coffee-chillatta">More info</a></p>
							<ul><div>Share your flavor</div> 
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.cinnabon.com/bakery-menu/chillatta/cinnamon-roll-coffee-chillatta"><img src="assets/img/facebook-logo.png" alt="Facebook"></a>
								<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/intent/tweet?text=Cinnamon%20Roll%20Coffee%20Chillatta%E2%84%A2&url=https%3A%2F%2Fwww.cinnabon.com%2Fbakery-menu%2Fchillatta%2Fcinnamon-roll-coffee-chillatta&hashtags=CinnamonRoll"><img src="assets/img/twitter-logo.png" alt="Twitter"></a></li>
								<li><a href="https://www.instagram.com/cinnabon/" target="_blank"><img src="assets/img/instagram-logo.png" alt="Instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="scroll-next"></div>
			<div class="scroll-next-shadow"></div>

		</article>

		<!-- nav squares -->
		<nav>
			<section id="cinnabon" data-attr="signature-cinnamon-roll-chillatta">
				<div class="toggle-trigger" data-attr="signature-cinnamon-roll-chillatta" id="trigger-cinnabon">
						
				</div>
				<div class="bg" id="img-cinnabon">
					
				</div>
			</section><section id="strawberry" data-attr="strawberries-cream-chillatta">
				<div class="toggle-trigger" data-attr="strawberries-cream-chillatta" id="trigger-strawberry">
						
				</div>
				<div class="bg" id="img-strawberry">
					
				</div>
			</section><section id="oreo"  data-attr="oreo-cookies-cream-chillatta">
				<div class="toggle-trigger" data-attr="oreo-cookies-cream-chillatta" id="trigger-oreo">
						
				</div>
				<div class="bg" id="img-oreo">
					
				</div>
			</section><section id="peach" data-attr="peaches-cream-chillatta">
				<div class="toggle-trigger" data-attr="peaches-cream-chillatta" id="trigger-peach">
						
				</div>
				<div class="bg" id="img-peach">
					
				</div>
			</section><section id="chocolate" data-attr="double-chocolate-mocha-chillatta">
				<div class="toggle-trigger" data-attr="double-chocolate-mocha-chillatta" id="trigger-chocolate">
						
				</div>
				<div class="bg" id="img-chocolate">
					
				</div>
			</section><section id="location">
				<a href="https://www.cinnabon.com/locations" target="_blank">
					<div class="bg">
						<div class="content">
							<img src="assets/img/pin.png" alt="Location Finder">
							<h2>Location<span>Finder</span></h2>
						</div>
					</div>
				</a>
			</section>
		</nav>
	</main>

	<footer>
		<p>© 2017 Cinnabon LLC.</p>
		<p class="showoreo">OREO<span class="regalt">®</span> and the OREO Wafer Design are registered trademarks of Mondelez International group, used with permission.</p>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<script src="js/scripts.js"></script>
</body>
</html>